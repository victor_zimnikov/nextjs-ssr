import React from "react";
import { isEmpty } from "lodash";
import { useQuery } from "react-apollo-hooks";

import { styled } from "../client/theme/AppTheme";
import { AppLayout } from "../client/components/app/AppLayout";
import { LoaderIcon } from "../client/components/icons/LoaderIcon";
import { ContainerBox } from "../client/components/ui/ContainerBox";
import { userQuery, UserQueryResult } from "../shared/queries/UserQuery";
import { GenderIcon, GenderType } from "../client/components/icons/GenderIcon";

const Loader = styled(LoaderIcon)`
  align-self: center;
`;

const UserRowBox = styled(ContainerBox)`
  flex-direction: row;
`;

const UserName = styled.div`
  display: flex;
  font-size: 28px;
  font-weight: bold;
  align-items: center;
  text-transform: uppercase;
`;

const UserId = styled.span`
  font-size: 18px;
  margin-left: 12px;
  font-weight: normal;
  text-transform: none;
`;

const UserLink = styled.a`
  margin-top: 6px;
`;

const UserCellBox = styled(ContainerBox)`
  margin-left: 24px;
`;

const UserText = styled.span`
  margin-top: 6px;
`;

export default () => {
  const { data, loading, error } = useQuery<UserQueryResult>(userQuery, {
    fetchPolicy: "no-cache",
  });

  const user = !isEmpty(data) ? data.payload : undefined;

  if (!loading && error) {
    return <div>{error.message}</div>;
  }

  return (
    <AppLayout>
      {loading && <Loader />}

      {!loading && user && (
        <>
          <UserRowBox>
            <img src={user.picture} />

            <UserCellBox>
              <UserName>
                <GenderIcon type={user.gender as GenderType} />

                {user.fullName}

                {user.id !== "" && <UserId>(#{user.id})</UserId>}
              </UserName>

              <UserLink href={`mailto:${user.email}`}>{user.email}</UserLink>

              <UserText>{user.phone}</UserText>

              <UserLink
                href={`https://yandex.ru/maps/?ll=${
                  user.location.coordinates.lon
                }%2C${user.location.coordinates.lat}&z=15`}
                target="_blank"
              >
                Location
              </UserLink>
            </UserCellBox>
          </UserRowBox>

          <UserRowBox />
        </>
      )}
    </AppLayout>
  );
};
