import React from "react";
import { isEmpty } from "lodash";
import ImageLoader from "react-loading-image";
import { useQuery } from "react-apollo-hooks";

import { styled } from "../client/theme/AppTheme";
import { AppLayout } from "../client/components/app/AppLayout";
import { LoaderIcon } from "../client/components/icons/LoaderIcon";
import { ContainerBox } from "../client/components/ui/ContainerBox";
import {
  portfolioQuery,
  ProtfolioQueryResult,
} from "../shared/queries/PortfolioQuery";

const RootBox = styled(ContainerBox)`
  flex-wrap: wrap;
  flex-direction: row;
  justify-content: center;
`;

const Loader = styled(LoaderIcon)`
  align-self: center;
`;

const ItemBox = styled.div`
  margin: 6px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Image = styled(ImageLoader)`
  width: 350px;
  height: auto;
`;

export default () => {
  const { data, loading, error } = useQuery<ProtfolioQueryResult>(
    portfolioQuery,
    {
      fetchPolicy: "no-cache",
    },
  );

  const portfolio = !isEmpty(data) ? data.payload : undefined;

  if (!loading && error) {
    return <div>{error.message}</div>;
  }

  return (
    <AppLayout>
      <RootBox>
        {loading && <Loader />}

        {!loading &&
          portfolio &&
          portfolio.list.map((x, idx) => (
            <ItemBox key={idx} title={x.author}>
              <Image src={x.picture} loading={() => <LoaderIcon size={12} />} />
            </ItemBox>
          ))}
      </RootBox>
    </AppLayout>
  );
};
