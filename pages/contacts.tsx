import React from "react";

import { AppLayout } from "../client/components/app/AppLayout";

export default () => (
  <AppLayout>
    <span>Contacts</span>
  </AppLayout>
);
