import React from "react";
import * as loremIpsum from "lorem-ipsum";

import { AppLayout } from "../client/components/app/AppLayout";

export default () => {
  const content = loremIpsum({
    count: 5,
    format: "html",
    units: "paragraphs",
  });

  return (
    <AppLayout title="Landing">
      <div dangerouslySetInnerHTML={{ __html: content }} />
    </AppLayout>
  );
};
