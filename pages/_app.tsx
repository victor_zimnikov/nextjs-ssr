import React from "react";
import Router from "next/router";
import { ApolloClient } from "apollo-client";
import { ApolloProvider } from "react-apollo";
import { ThemeProvider } from "styled-components";
import App, { AppProps, Container } from "next/app";
import { NormalizedCacheObject } from "apollo-boost";
import { ApolloProvider as ApolloHooksProvider } from "react-apollo-hooks";

import { MainTheme } from "../client/theme/MainTheme";
import withApolloClient from "../client/lib/withApolloClient";
import { DoneProgress, StartProgress } from "../client/helpers/Progress";
import {
  AppTheme,
  appTheme as defaultAppTheme,
} from "../client/theme/AppTheme";

Router.events.on("routeChangeError", () => DoneProgress());
Router.events.on("routeChangeStart", () => StartProgress());
Router.events.on("routeChangeComplete", () => DoneProgress());

interface Props extends AppProps {
  appTheme: AppTheme;
  apolloClient: ApolloClient<NormalizedCacheObject>;
}

class MyApp extends App<Props> {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps };
  }

  public render() {
    const {
      Component,
      pageProps,
      apolloClient,
      appTheme = defaultAppTheme,
    } = this.props;

    return (
      <Container>
        <ThemeProvider theme={appTheme}>
          <ApolloProvider client={apolloClient}>
            <ApolloHooksProvider client={apolloClient}>
              <MainTheme theme={appTheme} />
              <Component {...pageProps} />
            </ApolloHooksProvider>
          </ApolloProvider>
        </ThemeProvider>
      </Container>
    );
  }
}

export default withApolloClient(MyApp);
