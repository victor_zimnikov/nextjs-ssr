module.exports = {
  parser: "@typescript-eslint/parser",
  parserOptions: {
    ecmaVersion: 2018,
    project: "./tsconfig.json"
  },
  env: {
    es6: true,
    node: true,
    browser: true,
  },
  extends: [
    "prettier",
    "react-app",
    "airbnb-base",
    "plugin:import/errors",
    "plugin:import/warnings"
  ],
  settings: {
    "import/resolver": {
      "node": {
        "extensions": [
          ".js",
          ".ts",
          ".tsx",
        ]
      }
    }
  },
  plugins: ["@typescript-eslint", "react", "import"],
  rules: {
    "quotes": "off",
    "no-unused-vars": "off",
    "no-confusing-arrow": "off",
    "operator-linebreak": "off",
    "object-curly-newline": "off",
    "no-useless-constructor": "off",
    "import/no-named-default": "off",
    "implicit-arrow-linebreak": "off",
    "@typescript-eslint/no-unused-vars": "error",

    "arrow-parens": ["error", "as-needed"],

    //
    // eslint
    //

    // Just use Prettier ©
    "no-nested-ternary": "off",

    // Remind about technical debt.
    "no-warning-comments": [
      "warn",
      {
        location: "anywhere",
        terms: ["todo", "fixme"],
      },
    ],

    // Forbid to use built in logger.

    // Override `airbnb-eslint-config-base` warnings as errors.
    "no-alert": "error",
    "no-console": "error",
    "func-names": ["error", "always"],
    "no-constant-condition": "error",

    // Override `airbnb-eslint-config` defaults.
    "no-cond-assign": ["error", "except-parens"],

    // Allow empty catch blocks.
    "no-empty": ["error", { allowEmptyCatch: true }],

    // Allow plusplus in for loops.
    "no-plusplus": ["error", { allowForLoopAfterthoughts: true }],

    // It's ok to use hoisted functions before their actual declaration.
    "no-use-before-define": ["error", { functions: false }],

    // Force usage object spread over Object.assign.
    "prefer-object-spread": "error",

    // Potential bugs.
    "require-atomic-updates": "error",
    "no-async-promise-executor": "error",
    "no-misleading-character-class": "error",

    // Force usage of curly braces.
    curly: ["error", "all"],

    // Force newlines.
    "newline-after-var": "error",
    "newline-before-return": "error",

    //
    // eslint-plugin-import
    //

    // We use `sort-import` to deal with styles.
    "import/first": ["off", "absolute-first"],
    "import/order": ["off", { groups: [["builtin", "external", "internal"]] }],

    // We prefer named exports.
    "import/prefer-default-export": "off",
  },
};
