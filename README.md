## Server Side Rendering with NextJS

Example React SSR

```bash
yarn install
```

#### Development
```bash
yarn dev
```

#### Build
```bash
yarn build
```

#### Used
- React
- ExpressJS
- Typescript
- GraphQL
- Apollo
- NextJS
- NexusJS
- Styled Components
