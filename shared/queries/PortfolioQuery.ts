import gql from "graphql-tag";
import { NexusGenRootTypes } from "../../server/generated";

export const PortfolioFragment = gql`
  fragment ProtfolioFragment on PortfolioModelPage {
    totalCount
    list {
      author
      picture
    }
  }
`;

export const portfolioQuery = gql`
  ${PortfolioFragment}

  query PortfolioListModel {
    payload: portfolioListModel {
      ...ProtfolioFragment
    }
  }
`;

export interface ProtfolioQueryResult {
  payload: NexusGenRootTypes["PortfolioModelPage"];
}
