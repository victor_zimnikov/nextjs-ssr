import gql from "graphql-tag";
import { NexusGenRootTypes } from "../../server/generated";

export const CoordinatesFragment = gql`
  fragment CoordinatesFragment on Coordinates {
    lat
    lon
  }
`;

export const LocationFragment = gql`
  ${CoordinatesFragment}

  fragment LocationFragment on Location {
    city
    state
    street
    postcode

    coordinates {
      ...CoordinatesFragment
    }
  }
`;

export const UserFragment = gql`
  ${LocationFragment}

  fragment UserFragment on UserModel {
    id
    email
    phone
    gender
    picture
    fullName

    location {
      ...LocationFragment
    }
  }
`;

export const userQuery = gql`
  ${UserFragment}

  query UserModel {
    payload: userModel {
      ...UserFragment
    }
  }
`;

export interface UserQueryResult {
  payload: NexusGenRootTypes["UserModel"];
}
