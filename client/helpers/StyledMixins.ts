export type MediaBreakpoint = "xs" | "sm" | "md" | "lg" | "xl" | "fhd";

// Extra small devices
export const BREAKPOINT_EXTRA_SMALL = 320;

//  Small devices (landscape phones)
export const BREAKPOINT_SMALL = 576;

// Medium devices (tablets)
export const BREAKPOINT_MEDIUM = 768;

// Large devices (desktops)
export const BREAKPOINT_LARGE = 992;

// Extra large devices (large desktops)
export const BREAKPOINT_EXTRA_LARGE = 1200;

// Full HD devices (1920 * 1080 px)
export const BREAKPOINT_FULL_HD = 1920;

export function getBreakpointWidth(breakpoint: MediaBreakpoint): number {
  switch (breakpoint) {
    case "xs":
      return BREAKPOINT_EXTRA_SMALL;

    case "sm":
      return BREAKPOINT_SMALL;

    case "md":
      return BREAKPOINT_MEDIUM;

    case "lg":
      return BREAKPOINT_LARGE;

    case "xl":
      return BREAKPOINT_EXTRA_LARGE;

    case "fhd":
      return BREAKPOINT_FULL_HD;

    default:
      return 0;
  }
}

/**
 * Media of at least the minimum breakpoint width.
 */
export function mediaUp(breakpoint: MediaBreakpoint): string {
  return `@media (min-width: ${getBreakpointWidth(breakpoint)}px)`;
}

/**
 * Media of at most the maximum breakpoint width.
 */
export function mediaDown(breakpoint: MediaBreakpoint): string {
  return `@media (max-width: ${getBreakpointWidth(breakpoint) - 0.2}px)`;
}

/**
 * Media query for between the minimum and maximum breakpoint widths.
 */
export function mediaBetween(
  minBreakpoint: MediaBreakpoint,
  maxBreakpoint: MediaBreakpoint,
): string {
  return `@media (min-width: ${getBreakpointWidth(
    minBreakpoint,
  )}px) and (max-width: ${getBreakpointWidth(maxBreakpoint) - 0.2}px)`;
}

/**
 * Media query for exactly the size of breakpoint.
 */
export function mediaAt(breakpoint: MediaBreakpoint): string {
  return `@media (width: ${getBreakpointWidth(breakpoint)}px)`;
}
