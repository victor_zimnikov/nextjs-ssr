import "nprogress/nprogress.css";

import NProgress from "nprogress";

NProgress.configure({
  showSpinner: false,
  parent: "#__next",
});

const DoneProgress = () => NProgress.done();
const StartProgress = () => NProgress.start();

export { DoneProgress, StartProgress };
