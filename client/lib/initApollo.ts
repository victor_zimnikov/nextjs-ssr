import fetch from "isomorphic-unfetch";
import {
  HttpLink,
  ApolloClient,
  InMemoryCache,
  NormalizedCacheObject,
} from "apollo-boost";

let apolloClient = null;

if (!process.browser) {
  global.fetch = fetch;
}

const host = process.env.APP_HOST || "0.0.0.0";
const method = process.env.APP_METHOD || "http";
const port = Number(process.env.APP_GRAPHQL_PORT || 3010);

const APOLLO_LINK = `${method}://${host}:${port}`;

function create(initialState?: NormalizedCacheObject) {
  const cache = new InMemoryCache();

  if (initialState) {
    cache.restore(initialState);
  }

  return new ApolloClient({
    connectToDevTools: process.browser,
    ssrMode: !process.browser,
    link: new HttpLink({
      uri: APOLLO_LINK,
      credentials: "same-origin",
    }),
    cache,
  });
}

export function initApollo(initialState?: NormalizedCacheObject) {
  if (!process.browser) {
    return create(initialState);
  }

  if (!apolloClient) {
    apolloClient = create(initialState);
  }

  return apolloClient;
}
