import React from "react";
import Head from "next/head";
import { getDataFromTree } from "react-apollo";
import { NormalizedCacheObject } from "apollo-boost";

import { initApollo } from "./initApollo";
import { appTheme } from "../theme/AppTheme";
import { AppLogger } from "../../shared/helpers/AppLogger";

interface Props {
  apolloState: NormalizedCacheObject;
}

export default App =>
  class Apollo extends React.Component<Props> {
    static displayName = "withApollo(App)";

    static logger = new AppLogger("Apollo");

    static async getInitialProps(ctx) {
      const { Component, router } = ctx;

      let appProps = {};

      if (App.getInitialProps) {
        appProps = await App.getInitialProps(ctx);
      }

      const apollo = initApollo();

      if (!process.browser) {
        try {
          await getDataFromTree(
            <App
              {...appProps}
              router={router}
              appTheme={appTheme}
              Component={Component}
              apolloClient={apollo}
            />,
          );
        } catch (error) {
          this.logger.error(error);
        }

        Head.rewind();
      }

      const apolloState = apollo.cache.extract();

      return {
        ...appProps,

        apolloState,
      };
    }

    private apolloClient = initApollo(this.props.apolloState);

    render() {
      return <App {...this.props} apolloClient={this.apolloClient} />;
    }
  };
