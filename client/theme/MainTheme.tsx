import Head from "next/head";
import React, { ReactElement } from "react";

import { createGlobalStyle, css } from "./AppTheme";

const styles = css`
  *::before,
  *::after {
    box-sizing: border-box;
  }

  #__next {
    display: flex;
    min-height: 100vh;
    flex-direction: column;
  }

  html {
    &,
    body {
      margin: 0;
      padding: 0;

      font-size: 16px;
      color: ${({ theme }) => theme.colors.darkGray};
      font-family: "Montserrat", sans-serif !important;

      a,
      a:hover {
        text-decoration: none;
        color: ${({ theme }) => theme.palette.primary};
      }

      b,
      strong {
        font-weight: 500;
      }
    }
  }
`;

const Styles = createGlobalStyle`
  ${styles} 
`;

export function MainTheme({ theme }: any): null | ReactElement<object> {
  return (
    <Head>
      <Styles theme={theme} />

      <link
        rel="stylesheet"
        type="text/css"
        href="https://fonts.googleapis.com/css?family=Montserrat:400,500&subset=cyrillic"
      />
    </Head>
  );
}
