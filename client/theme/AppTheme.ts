import { transparentize } from "polished";
import * as styledComponents from "styled-components";

const {
  css,
  ThemeConsumer,
  default: styled,
  createGlobalStyle,
}: styledComponents.ThemedStyledComponentsModule<AppTheme> = styledComponents;

const colors = {
  white: "#fff",
  black: "#000",
  transparent: "transparent",

  azure: "#2cadd7",
  navyBlue: "#002257",

  red: "#dc3545",
  green: "#279f7c",

  gray: "#c7d1d8",
  lightGray: "#eff1f4",
  paleGray: "#a0a5b5",
  lightBlueGray: "#d6d8da",
  blueGray: "#7f90aa",
  anotherGray: "#f6f6f6",
  yetAnotherGray: "#f9f9f9",
  articleGray: "#404040",
  darkGray: "#3a494c",
};

const palette = {
  danger: colors.red,
  primary: colors.green,
  secondary: colors.azure,
  light: colors.lightBlueGray,
};

export type ThemePalette = typeof palette;
export type ThemePaletteVariant = keyof ThemePalette;

export type BadgeTheme = {
  readonly [TKey in keyof ThemePalette]: {
    readonly textColor: string;
    readonly backgroundColor: string;
  }
};

const badge: BadgeTheme = {
  primary: {
    textColor: colors.white,
    backgroundColor: palette.primary,
  },

  secondary: {
    textColor: colors.white,
    backgroundColor: palette.secondary,
  },

  light: {
    textColor: palette.primary,
    backgroundColor: transparentize(0.77, palette.light),
  },

  danger: {
    textColor: colors.white,
    backgroundColor: palette.danger,
  },
};

export type ButtonTheme = {
  readonly [TKey in keyof ThemePalette]: {
    readonly textColor: string;
    readonly shadowColor: string;
    readonly backgroundColor: string;
  }
};

const button: ButtonTheme = {
  primary: {
    textColor: colors.white,
    backgroundColor: palette.primary,
    shadowColor: transparentize(0.5, palette.primary),
  },

  secondary: {
    textColor: colors.white,
    backgroundColor: palette.secondary,
    shadowColor: transparentize(0.5, palette.secondary),
  },

  light: {
    textColor: palette.primary,
    shadowColor: transparentize(0.5, palette.light),
    backgroundColor: transparentize(0.77, palette.light),
  },

  danger: {
    textColor: colors.white,
    backgroundColor: palette.danger,
    shadowColor: transparentize(0.5, palette.danger),
  },
};

export const appTheme = { colors, palette, badge, button };

export type AppTheme = typeof appTheme;

export { css, styled, createGlobalStyle, ThemeConsumer };
