import Head from "next/head";
import React, { ReactNode } from "react";
import { AppLayoutHeader } from "./AppLayoutHeader";
import { AppLayoutFooter } from "./AppLayoutFooter";
import { ContainerBox } from "../ui/ContainerBox";
import { styled } from "../../theme/AppTheme";

const ContentBox = styled(ContainerBox)`
  flex: 1 1 0%;
  padding: 24px 0;
`;

const siteName = "SiteName";

interface Props {
  title?: string;
  children: ReactNode;
}

export function AppLayout({ children, title }: Props) {
  const titleText = !title ? siteName : `${title} | ${siteName}`;

  return (
    <>
      <Head>
        <title>{titleText}</title>

        <meta property="og:title" content={titleText} />
      </Head>

      <AppLayoutHeader />

      <ContentBox>{children}</ContentBox>

      <AppLayoutFooter />
    </>
  );
}
