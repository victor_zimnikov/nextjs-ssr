import React from "react";
import { styled } from "../../theme/AppTheme";

const FooterBox = styled.div`
  display: flex;
  max-width: 100%;
  min-height: 150px;
  padding-top: 32px;
  flex-direction: row;
  color: ${({ theme }) => theme.colors.white};
  background-color: ${({ theme }) => theme.palette.primary};
`;

export const AppLayoutFooter = () => <FooterBox />;
