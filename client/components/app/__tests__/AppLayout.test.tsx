/* eslint-env jest */
import "../../../../shared/setupTests";

import React from "react";
import { shallow } from "enzyme";

import { AppLayout } from "../AppLayout";

describe("AppLayout", () => {
  it("renders the span text", () => {
    const appLayout = shallow(
      <AppLayout>
        <span>Test Text</span>
      </AppLayout>,
    );
    expect(appLayout.find("span").text()).toEqual("Test Text");
  });

  it("set prop `title`", () => {
    const appLayout = shallow(
      <AppLayout title="Test Title Text">
        <span>Test Text</span>
      </AppLayout>,
    );
    expect(appLayout.find("title").text()).toEqual(
      "Test Title Text | SiteName",
    );
  });
});
