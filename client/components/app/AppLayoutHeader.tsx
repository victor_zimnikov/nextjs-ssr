import React from "react";

import { Link } from "../ui/Link";
import { styled } from "../../theme/AppTheme";
import { ContainerBox } from "../ui/ContainerBox";

const FooterBox = styled(ContainerBox)`
  height: 75px;
  margin-top: 42px;
  align-items: center;
  flex-direction: row;
  justify-content: space-between;
`;

const NavLink = styled(Link)`
  margin: 0 16px;
  font-size: 16px;
  font-weight: 700;
  text-decoration: none;
  color: ${({ theme }) => theme.colors.darkGray};

  &:hover {
    text-decoration: underline;
  }
`;

const Logo = styled.span`
  font-size: 32px;
  font-weight: 700;
  color: ${({ theme }) => theme.palette.primary};
`;

export const AppLayoutHeader = () => (
  <FooterBox>
    <Logo>Freelance</Logo>

    <nav>
      <NavLink href="/" title="Home" />
      <NavLink href="/about" title="About Me" />
      <NavLink href="/portfolio" title="Portfolio" />
      <NavLink href="/contacts" title="Contacts" />
    </nav>
  </FooterBox>
);
