import { Omit } from "utility-types";
import React, { ReactElement } from "react";
import { default as NextLink, LinkProps } from "next/link";

interface Props extends Omit<LinkProps, "children"> {
  className?: string;
  title: ReactElement<object> | string;
}

export const Link = ({ className, title, ...props }: Props) => (
  <NextLink {...props}>
    {React.isValidElement(title) ? (
      React.cloneElement<{ className?: string }>(title, { className })
    ) : (
      <a className={className}>{title}</a>
    )}
  </NextLink>
);
