import React from "react";

interface Props {
  size?: number;
  className?: string;
}

export const LoaderIcon = ({ size = 50, className }: Props) => (
  <svg
    width={`${size}px`}
    height={`${size}px`}
    viewBox="0 0 100 100"
    className={className}
    preserveAspectRatio="xMidYMid"
    xmlns="http://www.w3.org/2000/svg"
  >
    <circle
      r="40"
      cx="50"
      cy="50"
      fill="none"
      strokeWidth="4"
      stroke="#279f7c"
      strokeLinecap="round"
      transform="rotate(238.709 50 50)"
      strokeDasharray="62.83185307179586 62.83185307179586"
    >
      <animateTransform
        dur="1s"
        begin="0s"
        type="rotate"
        keyTimes="0;1"
        calcMode="linear"
        repeatCount="indefinite"
        attributeName="transform"
        values="0 50 50;360 50 50"
      />
    </circle>
    <circle
      r="35"
      cx="50"
      cy="50"
      fill="none"
      strokeWidth="4"
      stroke="#18dda3"
      strokeLinecap="round"
      transform="rotate(-238.709 50 50)"
      strokeDashoffset="54.97787143782138"
      strokeDasharray="54.97787143782138 54.97787143782138"
    >
      <animateTransform
        dur="1s"
        begin="0s"
        type="rotate"
        keyTimes="0;1"
        calcMode="linear"
        repeatCount="indefinite"
        attributeName="transform"
        values="0 50 50;-360 50 50"
      />
    </circle>
  </svg>
);
