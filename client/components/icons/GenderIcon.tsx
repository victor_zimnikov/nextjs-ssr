import React from "react";
import { ThemeConsumer } from "../../theme/AppTheme";

export type GenderType = "female" | "male";

interface Props {
  size?: number;
  type: GenderType;
  className?: string;
}

export const GenderIcon = ({ size = 24, className, type }: Props) => (
  <ThemeConsumer>
    {({ colors }) =>
      type === "male" ? (
        <svg
          className={className}
          width={`${size}px`}
          height={`${size}px`}
          viewBox="0 0 24 24"
        >
          <path
            fill={colors.navyBlue}
            d="M9,9C10.29,9 11.5,9.41 12.47,10.11L17.58,5H13V3H21V11H19V6.41L13.89,11.5C14.59,12.5 15,13.7 15,15A6,6 0 0,1 9,21A6,6 0 0,1 3,15A6,6 0 0,1 9,9M9,11A4,4 0 0,0 5,15A4,4 0 0,0 9,19A4,4 0 0,0 13,15A4,4 0 0,0 9,11Z"
          />
        </svg>
      ) : (
        <svg
          className={className}
          width={`${size}px`}
          height={`${size}px`}
          viewBox="0 0 24 24"
        >
          <path
            fill={colors.red}
            d="M12,4A6,6 0 0,1 18,10C18,12.97 15.84,15.44 13,15.92V18H15V20H13V22H11V20H9V18H11V15.92C8.16,15.44 6,12.97 6,10A6,6 0 0,1 12,4M12,6A4,4 0 0,0 8,10A4,4 0 0,0 12,14A4,4 0 0,0 16,10A4,4 0 0,0 12,6Z"
          />
        </svg>
      )
    }
  </ThemeConsumer>
);
