declare namespace NodeJS {
  interface Process {
    browser: boolean;
  }

  interface Global {
    fetch: (input: RequestInfo, init?: RequestInit) => Promise<Response>;
  }
}
