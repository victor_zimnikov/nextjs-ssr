const { APP_PORT, NODE_ENV, APP_GRAPHQL_PORT } = process.env;

export const IS_DEV = NODE_ENV !== "production";

export const WEB_PORT = Number(APP_PORT || 3000);
export const GRAPH_SERVER_PORT = Number(APP_GRAPHQL_PORT || 3010);
