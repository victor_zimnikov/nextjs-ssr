import { AbstractApi } from "./AbstractApi";

export class UserApi extends AbstractApi {
  public getUser(): Promise<object> {
    return this.get("https://randomuser.me/api/");
  }
}
