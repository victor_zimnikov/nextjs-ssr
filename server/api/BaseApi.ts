import { UserApi } from "./UserApi";
import { PortfolioApi } from "./PortfolioApi";

export class BaseApi {
  public userApi = new UserApi();

  public portfolioApi = new PortfolioApi();
}
