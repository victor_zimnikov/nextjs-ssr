import axios, { AxiosResponse } from "axios";

export class AbstractApi {
  // eslint-disable-next-line
  public get(url: string): Promise<AxiosResponse<object>> {
    return axios(url, {
      method: "GET",
    });
  }
}
