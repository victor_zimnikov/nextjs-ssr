import { AbstractApi } from "./AbstractApi";

export class PortfolioApi extends AbstractApi {
  public getProtfolioList(): Promise<object> {
    return this.get("https://picsum.photos/list");
  }
}
