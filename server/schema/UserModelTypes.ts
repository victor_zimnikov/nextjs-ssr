import "../generated";

import { get } from "lodash";
import { AxiosResponse } from "axios";
import { extendType, objectType } from "nexus";

export const CoordinatesType = objectType({
  name: "Coordinates",
  definition(type) {
    type.string("lat");
    type.string("lon");
  },
});

export const LocationType = objectType({
  name: "Location",
  definition(type) {
    type.string("city");
    type.string("state");
    type.string("street");
    type.string("postcode");
    type.field("coordinates", { type: "Coordinates" });
  },
});

export const UserModelType = objectType({
  name: "UserModel",
  definition(type) {
    type.string("gender");
    type.string("fullName");
    type.string("email");
    type.string("phone");
    type.string("id");
    type.string("picture");
    type.field("location", { type: "Location" });
  },
});

export const UserModelQueryType = extendType({
  type: "Query",
  definition(type) {
    type.field("userModel", {
      type: "UserModel",
      resolve: async (_, __, { api }) =>
        api.userApi
          .getUser()
          .then(({ data }: AxiosResponse<{ results: object }>) => {
            const user = get(data, "results.0", {});

            return {
              id: get(user, "id.value") || "",
              email: get(user, "email", ""),
              fullName: [
                get(user, "name.first", ""),
                get(user, "name.last", ""),
              ].join(" "),
              gender: get(user, "gender", ""),
              location: {
                city: get(user, "location.city", ""),
                coordinates: {
                  lat: get(user, "location.coordinates.latitude", ""),
                  lon: get(user, "location.coordinates.longitude", ""),
                },
                postcode: get(user, "location.postcode", ""),
                state: get(user, "location.state", ""),
                street: get(user, "location.street", ""),
              },
              phone: get(user, "phone", ""),
              picture: get(user, "picture.large", ""),
            };
          }),
    });
  },
});
