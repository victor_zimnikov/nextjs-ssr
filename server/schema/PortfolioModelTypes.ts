import "../generated";

import { get } from "lodash";
import { AxiosResponse } from "axios";
import { extendType, objectType } from "nexus";

export const ProtfolioModelType = objectType({
  name: "PortfolioModel",
  definition(type) {
    type.string("author");
    type.string("picture");
  },
});

export const PortfolioModelPageType = objectType({
  name: "PortfolioModelPage",
  definition(type) {
    type.int("totalCount");
    type.field("list", { type: "PortfolioModel", list: true });
  },
});

export const PortfolioModelQueryType = extendType({
  type: "Query",
  definition(type) {
    type.field("portfolioListModel", {
      type: "PortfolioModelPage",
      resolve: async (_, __, { api }) =>
        api.portfolioApi
          .getProtfolioList()
          .then(({ data = [] }: AxiosResponse<[]>) => {
            const startIndex = Math.floor(Math.random() * (data.length - 10));

            const list = data.slice(startIndex, startIndex + 10).map(x => ({
              author: get(x, "author", ""),
              picture: `https://source.unsplash.com/collection/${get(
                x,
                "id",
                0,
              )}/400x300`,
            }));

            return {
              list,
              totalCount: list.length,
            };
          }),
    });
  },
});
