import { objectType, scalarType } from "nexus";

export const FloatScalarType = scalarType({
  name: "Float",
  serialize: value => value,
});

export const DateScalarType = scalarType({
  name: "Date",
  asNexusMethod: "date",
  serialize: value => value,
});

export const UploadScalarType = scalarType({
  name: "Upload",
  parseValue: value => value,

  parseLiteral() {
    throw new Error("‘Upload’ scalar literal unsupported.");
  },

  serialize() {
    throw new Error("‘Upload’ scalar serialization unsupported.");
  },
});

export const RootQuerySchema = objectType({
  name: "Query",
  definition(type) {
    type.string("ping", { resolve: () => "pong" });
  },
});

export const RootMutationSchema = objectType({
  name: "Mutation",
  definition(type) {
    type.string("ping", { resolve: () => "pong" });
  },
});

export const RootSubscriptionSchema = objectType({
  name: "Subscription",
  definition(type) {
    type.string("ping", { resolve: () => "pong" });
  },
});
