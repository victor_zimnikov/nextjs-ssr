import * as path from "path";
import { makeSchema } from "nexus";

import * as RootTypes from "./RootTypes";
import * as UserModelTypes from "./UserModelTypes";
import * as PortfolioModelTypes from "./PortfolioModelTypes";

import { IS_DEV } from "../constants/ServerConstants";

const generatedDir = IS_DEV
  ? path.join(__dirname, "..", "generated")
  : __dirname;

export const NexusSchema = makeSchema({
  types: [RootTypes, UserModelTypes, PortfolioModelTypes],

  outputs: {
    typegen: path.join(generatedDir, "index.ts"),
    schema: path.join(generatedDir, "schema.graphql"),
  },

  typegenAutoConfig: {
    contextType: "t.RequestContext",
    sources: [
      {
        alias: "t",
        source: path.join(__dirname, "..", "helpers", "RequestContext.ts"),
      },
    ],
  },
});
