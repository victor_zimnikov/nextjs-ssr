import { Server } from "next";
import * as express from "express";

import { ServerLogger } from "./ServerLogger";

import { WEB_PORT } from "../constants/ServerConstants";

// TODO: Refactoring as promise
export function createWebServer(app: Server) {
  const logger = new ServerLogger("Web server");

  const handle = app.getRequestHandler();

  const webServer = express();

  webServer.get("*", (req, res) => handle(req, res));

  webServer.listen(WEB_PORT, error => {
    if (error) {
      logger.error(error);

      return;
    }

    logger.log(`Web-server is starting on port: ${WEB_PORT}`);
  });
}
