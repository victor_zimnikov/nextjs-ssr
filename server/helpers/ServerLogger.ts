import debug, { IDebugger } from "debug";

export class ServerLogger {
  public static enableLogger(...namespaces: string[]): void {
    debug.enable(namespaces.join(","));
  }

  private readonly logger?: IDebugger;

  public constructor(namespace: string) {
    this.logger = debug(`App:${namespace}`);
  }

  public log(message: string, ...args: unknown[]): void {
    if (this.logger) {
      this.logger(message, ...args);
    }
  }

  public error(error?: Error) {
    if (this.logger) {
      if (error) {
        if (error.message) {
          this.logger(error.message);
        } else {
          this.logger(error);
        }

        if (error.stack) {
          // eslint-disable-next-line no-console
          console.error(error.stack);
        }
      }
    }
  }
}
