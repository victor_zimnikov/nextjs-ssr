import { GraphQLServer } from "graphql-yoga";

import { ServerLogger } from "./ServerLogger";
import { NexusSchema } from "../schema/NexusSchema";

import { GRAPH_SERVER_PORT, IS_DEV } from "../constants/ServerConstants";
import { RequestContext } from "./RequestContext";

// TODO: Refactoring as promise
export function createGraphQLServer() {
  const logger = new ServerLogger("GraphQL server");

  const server = new GraphQLServer({
    schema: NexusSchema,
    context: () => new RequestContext(),
  });

  server
    .start(
      {
        playground: IS_DEV ? "/" : false,
        cors: {
          origin: "*",
        },
        port: GRAPH_SERVER_PORT,
      },
      () => {
        logger.log(`GraphQL server is starting on port: ${GRAPH_SERVER_PORT}`);
      },
    )
    .catch(error => logger.error(error));
}
