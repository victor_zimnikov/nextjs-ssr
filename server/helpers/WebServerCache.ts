import { Server } from "next";
import * as LRUCache from "lru-cache";
import { Express, Request, Response } from "express";

import { ServerLogger } from "./ServerLogger";
import { IS_DEV } from "../constants/ServerConstants";

export class WebServerCache {
  private logger = new ServerLogger("Web server cache");

  private ssrCache = new LRUCache({
    max: 100,
    maxAge: IS_DEV ? 100 : 1000 * 60 * 60,
  });

  private getCacheKey = (request: Request) => request.url;

  public renderAndCache(
    request: Request,
    response: Response,
    pagePath,
    queryParams?: object,
  ) {
    const key = this.getCacheKey(request);

    // If we have a page in the cache, let's serve it
    if (this.ssrCache.has(key)) {
      if (IS_DEV) {
        this.logger.log(`CACHE HIT: ${key}`);
      }

      response.send(this.ssrCache.get(key));

      return;
    }

    // If not let's render the page into HTML
    this.nextServer
      // @ts-ignore
      .renderToHTML(request, response, pagePath, queryParams)
      .then((html: string) => {
        // Let's cache this page
        if (IS_DEV) {
          this.logger.log(`CACHE MISS: ${key}`);
        }

        this.ssrCache.set(key, html);

        response.send(html);
      })
      .catch((err: Error) => {
        this.nextServer.renderError(
          err,
          request,
          response,
          pagePath,
          // @ts-ignore
          queryParams,
        );
      });
  }

  public constructor(
    private readonly expressServer: Express,
    private readonly nextServer: Server,
  ) {
    // Noop
  }
}
