import * as path from "path";
import * as next from "next";

import { ServerLogger } from "./helpers/ServerLogger";
import { createWebServer } from "./helpers/WebServer";
import { createGraphQLServer } from "./helpers/GraphQLServer";

import { IS_DEV } from "./constants/ServerConstants";

ServerLogger.enableLogger("App:*");

const app = next({
  dev: IS_DEV,
  dir: path.resolve(__dirname, ".."),
});

app.prepare().then(() => {
  createGraphQLServer();

  createWebServer(app);
});
